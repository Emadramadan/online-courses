require("express-async-errors");
require("dotenv").config({ path: "./config.env" });
const express = require("express");
const cookieParser = require('cookie-parser');
const AppError = require("./app/middleware/classError");
const error = require("./app/middleware/error");
const routes = require("./startup/routes");
const connectDB = require("./startup/db");

// Create express app
const app = express();

// Connect to database
connectDB();

// Parse cookies
app.use(cookieParser());

// Add routes
routes(app);

// Handle 404 errors
app.all("*", (req, res, next) => {
  next(new AppError(`Can't find ${req.originalUrl} on this server`, 404));
});

// Handle errors
app.use(error);

// Start server
const port = process.env.PORT || 3000;
const server = app.listen(port, () =>
  console.log(`Server is running on port ${port}`)
);

// Handle unhandled rejections and uncaught exceptions
process.on("unhandledRejection", (err) => {
  console.log("Unhandled rejection: DB shutting down...");
  console.log(err.name, err.message);
  server.close(() => {
    process.exit(1);
  });
});

process.on("uncaughtException", (err) => {
  console.log("Uncaught exception: shutting down...");
  console.log(err.name, err.message);
  server.close(() => {
    process.exit(1);
  });
});
