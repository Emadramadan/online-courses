const mongoose = require("mongoose");

module.exports = function () {
  mongoose
    .connect(process.env.CONNECT_DB)
    .then(() => console.log("connected to Mongodb..."))
    .catch((err) => console.log("could not connect to mongodb...", err));
};
