const express = require("express");
const helmet = require("helmet");
const morgan = require("morgan");
const rateLimit = require("express-rate-limit");
const mongoSanitize = require("express-mongo-sanitize");

const courses = require("../app/routes/courseRoute");
const authors = require("../app/routes/authorRoute");
const auth = require("../app/routes/authRoute");

const limiter = rateLimit({
  max: 1000,
  windowMs: 60 * 60 * 1000,
  message: "Too many requests from this IP, please try again in an hour"
});

module.exports = function (app) {
  // Apply rate limiting to all routes under /api
  app.use("/api", limiter);

  // Remove potentially malicious data from request bodies
  app.use(express.json());
  app.use(mongoSanitize());

  // Use Morgan logging in development mode
  if (process.env.NODE_ENV === "development") {
    app.use(morgan("dev"));
  }

  // Use Helmet to set security-related HTTP headers
  app.use(helmet());

  // Add routes for courses, authors, and authentication
  app.use("/api/courses", courses);
  app.use("/api/authors", authors);
  app.use("/api/auth", auth);
};