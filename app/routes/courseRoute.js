const auth = require("../middleware/auth");
const { restrictTo } = require("../middleware/roles");
const express = require("express");
const router = express.Router();
const { getCourses, createCourse, updateCourse, deleteCourse, getCourse, }
  = require("../controller/courseController");

router.route("/").get(getCourses).post([auth, restrictTo("user", "admin")], createCourse);

router.route("/:id").get(getCourse).patch([auth], updateCourse).delete([auth], deleteCourse);

module.exports = router;
