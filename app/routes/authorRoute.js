const express = require("express");
const router = express.Router();
const { getAuthors, createAuthor, updateAuthor, deleteAuthor, getAuthor, }
  = require("../controller/authorController");
const auth = require("../middleware/auth");

router.route("/").get(getAuthors).post([auth], createAuthor);

router.route("/:id").get(getAuthor).patch([auth], updateAuthor).delete([auth], deleteAuthor);

module.exports = router;
