const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");
const { login, signup, updatePassword } = require("../controller/authController");
const { getAllUsers, updateUser, deleteUser } = require("../controller/userController");

router.post("/login", login);
router.post("/signup", signup);

router.patch("/updatePassword", [auth], updatePassword);

router.route("/getAllUsers").get(getAllUsers);
router.route("/profile/updateUser").patch([auth], updateUser);
router.route("/deleteUser").patch([auth], deleteUser);

module.exports = router;
