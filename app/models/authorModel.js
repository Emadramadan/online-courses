const mongoose = require("mongoose");

const authorSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      minlength: [3, "Please enter at least 3 characters"],
      maxlength: [50, "Maximum of 75 characters exceeded"],
      trim: true,
      lowercase: true,
      required: [true, "name is required"],
    },
    bio: {
      type: String,
      minlength: [3, "Please enter at least 3 characters"],
      maxlength: [255, "Maximum of 255 characters exceeded"],
      trim: true,
      lowercase: true,
      required: [true, "bio is required"],
    },
    website: {
      type: String,
      minlength: [3, "Please enter at least 3 characters"],
      maxlength: [50, "Maximum of 50 characters exceeded"],
      trim: true,
      lowercase: true,
      required: [true, "website is required"],
    },
  },
  { timestamps: true }
);

const Author = mongoose.model("Author", authorSchema);


module.exports = { Author };
