const mongoose = require("mongoose");

const schema = new mongoose.Schema(
  {
    title: {
      type: String,
      minlength: [3, "Please enter at least 3 characters"],
      maxlength: [75, "Maximum of 75 characters exceeded"],
      trim: true,
      lowercase: true,
      required: [true, "name is required"],
    },
    description: {
      type: String,
      minlength: [3, "Please enter at least 3 characters"],
      maxlength: [255, "Maximum of 255 characters exceeded"],
      trim: true,
      required: [true, "description is required"],
    },
    price: {
      type: Number,
      max: 999999,
      min: 0,
      required: [true, "price is required"],
    },
    author: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Author",
      required: [true, "must to be add Author"],
    },
  },
  { timestamps: true }
);

const Course = mongoose.model("Courses", schema);


module.exports = { Course };
