const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const schemaUsers = new mongoose.Schema({
  username: {
    type: String,
    minlength: [5, "Please enter at least 5 characters"],
    maxlength: [255, "Maximum of 255 characters exceeded"],
    trim: true,
    lowercase: true,
    // unique: [true, "username must be uniqe"],
    required: [true, "username is required"],
  },
  email: {
    type: String,
    minlength: [5, "Please enter at least 5 characters"],
    maxlength: [255, "Maximum of 255 characters exceeded"],
    trim: true,
    lowercase: true,
    unique: [true, "email must be uniqe"],
    required: [true, "email is required"],
  },
  password: {
    type: String,
    minlength: [5, "Please enter at least 5 characters"],
    maxlength: [1024, "Maximum of 1024 characters exceeded"],
    required: [true, "password is required"],
  },
  passwordConfirm: {
    type: String,
    required: true,
    validate: {
      // this only works on CREATE and SAVE
      validator: function (v) {
        return v === this.password;
      },
      message: 'Confirm password must match password'
    }
  },
  // passwordChangedAt: Date,
  role: { type: String, enum: ["user", "admin"], default: "user" },
  active: { type: Boolean, default: true }
},
  { timestamps: true },
);

schemaUsers.pre(/^find/, function (next) {
  // get All note equle active = true
  this.find({ active: { $ne: false } });
  next()
});

schemaUsers.pre("save", async function (next) {
  if (!this.isModified("password")) return next();
  this.password = await bcrypt.hash(this.password, 12);
  this.passwordConfirm = undefined;
  next();
});

schemaUsers.methods.generateAuthToken = function () {
  const token = jwt.sign({ id: this._id, role: this.role },
    process.env.ACCESS_TOKEN_SECRET, { expiresIn: process.env.JWT_EXPIRES_IN });
  return token;
};

// schemaUsers.method.changePasswordAfter = function (JWTTimestamp) {
//   if (this.passwordChangedAt) {
//     const changedTimestamp = parent(this.passwordChangedAt.getTime() / 1000);
//     return JWTTimestamp < changedTimestamp;
//   }
//   return false;
// }

const User = mongoose.model("User", schemaUsers);


module.exports = { User };
