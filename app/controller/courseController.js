// const asyncMiddleware = require("../middleware/async");
const _ = require("lodash");
const { Course } = require("../models/courseModel");
const { validationCreateCourse, validationUpdateCourse } = require("../middleware/errorJoi");
const AppError = require("../middleware/classError");


// get all courses
const getCourses = async (req, res, next) => {
  const page = req.query.page * 1 || 1;
  const limit = req.query.limit * 1 || 5;
  const skip = (page - 1) * limit;
  // {1} Find all courses in the database, populate author details, exclude certain fields, and sort by title
  const getCourses = await Course.find()
    .limit(limit)
    .skip(skip)
    .populate({ path: "author", select: "-__v -_id" })
    .sort({ title: 1 })
    .select("-__v");
  if (getCourses.length == 0) { return next(new AppError("Don't Have Courses", 400)); }
  // {2} Return a 200 response
  res.status(200).json({ status: "success", count: getCourses.length, page, data: { result: getCourses } });
};

// create new course
const createCourse = async (req, res, next) => {
  const { error } = validationCreateCourse(req.body);
  if (error) { return res.status(400).json({ status: "fail", message: error.message }); }
  // {1} Create a new course in the database
  const createNewCourse = await Course.create(_.pick(req.body, ["title", "description", "price", "author"]));
  // {2} Return a 200 response
  res.status(200).json({ status: "success", data: { result: createNewCourse } });
};

// update course
const updateCourse = async (req, res, next) => {
  const { error } = validationUpdateCourse(req.body);
  if (error) { return res.status(400).json({ status: "fail", message: error.message, }); }
  // {1} Find and update an existing course in the database
  const updateCourse = await Course.findByIdAndUpdate(
    req.params.id, _.pick(req.body, ["title", "description", "price", "author"]),
    { new: true, runValidators: true, });
  if (!updateCourse) { return next(new AppError("course not found", 404)); }
  // {2} Return a 200 response
  res.status(200).json({ status: "success", data: { result: updateCourse } });
};

// remove course
const deleteCourse = async (req, res, next) => {
  // {1} Find and remove an existing course from the database by ID
  const removeCourse = await Course.findByIdAndRemove(req.params.id);
  if (!removeCourse) { return next(new AppError("course not found", 404)); }
  // {2} Return a 200 response
  res.status(200).json({ status: "success", data: { result: removeCourse } });
};

// get course by id
const getCourse = async (req, res, next) => {
  // {1} Find a course in the database by ID, populate author details
  const getOneCourse = await Course.findById(req.params.id)
    .populate({ path: "author", select: "-__v -_id" }).select("-__v");
  if (!getOneCourse) { return next(new AppError("course not found", 404)); }
  // {2} Return a 200 response
  res.status(200).json({ status: "success", data: { result: getOneCourse } });
};

module.exports = { getCourses, createCourse, updateCourse, deleteCourse, getCourse, };
