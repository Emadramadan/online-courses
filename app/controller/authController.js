const _ = require("lodash");
const { User } = require("../models/userModel");
const { validateUserSignup, validateUserLogin, validateUserUpdatePass } = require("../middleware/errorJoi");
const bcrypt = require("bcryptjs");
const AppError = require("../middleware/classError");

const signup = async (req, res, next) => {
  // Validate incoming user data
  const { error } = validateUserSignup(req.body);
  if (error) {
    return res.status(400).json({ status: "fail", message: error.message });
  }
  // {1} Check if user already exists
  const user = await User.findOne({ email: req.body.email });
  if (user) { return next(new AppError("user already registered", 400)); }
  // {2} If user doesn't exist, create a new user using the incoming data
  const createUser = await User.create(_.pick(req.body, ["username", "email", "password", "passwordConfirm"]));
  // {3} Generate a JSON Web Token (JWT) for the new user
  const token = createUser.generateAuthToken();
  // {4} Set the JWT as a cookie on the response
  res.cookie("authorization", token, {
    expires: new Date(Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000),
    httpOnly: true
  });
  // {5} Return a 200 response with the JWT and some user information
  res.status(200).json({
    status: "success", token, result: _.pick(createUser, ["_id", "username", "email"]),
  });
};

const login = async (req, res, next) => {
  // Validate incoming login data
  const { error } = validateUserLogin(req.body);
  if (error) {
    return res.status(400).json({ status: "fail", message: error.message });
  }
  // {1} Find user with provided email
  const user = await User.findOne({ email: req.body.email });
  if (!user) { return next(new AppError("Invalid email", 400)); }
  // {2} Compare passwords
  const validPassword = await bcrypt.compare(req.body.password, user.password);
  if (!validPassword) { return next(new AppError("Invalid password", 400)); }
  // {3} Generate a JSON Web Token (JWT) for the user
  const token = user.generateAuthToken();
  // {4} Set the JWT as a cookie on the response
  res.cookie("authorization", token, {
    expires: new Date(Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000),
    httpOnly: true
  });
  // {5} Return a 200 response with the JWT and some user information
  res.status(200).json({ status: "success", result: _.pick(user, ["_id", "username", "email"]), token });
};

const updatePassword = async (req, res, next) => {
  const { error } = validateUserUpdatePass(req.body);
  if (error) {
    return res.status(400).json({ status: "fail", message: error.message });
  }
  // {1} Find user by ID from authenticated request
  const user = await User.findById(req.user.id);
  // {2} Compare passwords
  const validPassword = await bcrypt.compare(req.body.passwordCurrent, user.password);
  if (!validPassword) { return next(new AppError("Invalid password", 401)); }
  if (req.body.password === req.body.passwordCurrent) {
    return next(new AppError("The password is the same as the previous one.", 400));
  }
  // {3} Update user's password and password confirmation
  user.password = req.body.password;
  user.passwordConfirm = req.body.passwordConfirm;
  await user.save();
  // {4} Generate a JSON Web Token (JWT) for the user
  const token = user.generateAuthToken();
  // {5} Return a 200 response with the user information and the JWT
  res.status(200).json({ status: "success", token, result: _.pick(user, ["_id", "username", "email"]), token });
}

module.exports = { login, signup, updatePassword };
