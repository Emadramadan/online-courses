const _ = require("lodash");
const { Author } = require("../models/authorModel");
const { validationCreateAuthor, validationUpdateAuthor } = require("../middleware/errorJoi");
const AppError = require("../middleware/classError");

// get all Authors
const getAuthors = async (req, res, next) => {
  const page = req.query.page * 1 || 1;
  const limit = req.query.limit * 1 || 5;
  const skip = (page - 1) * limit;
  // {1} Find all authors in the database
  const getAuthors = await Author.find()
    .limit(limit)
    .skip(skip)
    .sort({ name: 1 })
    .select("name bio website _id  createdAt updatedAt");
  if (getAuthors.length == 0) return next(new AppError("Don't Have Authors", 400));
  // {2} Return a 200 response 
  res.status(200).json({ status: "success", count: getAuthors.length, page, data: { result: getAuthors } });
};

// create new Author
const createAuthor = async (req, res, next) => {
  const { error } = validationCreateAuthor(req.body);
  if (error) { return res.status(400).json({ status: "fail", message: error.message, }); }
  // {1} Create a new author in the database
  const createNewAuthor = await Author.create(_.pick(req.body, ["name", "bio", "website"]));
  // {2} Return a 200 response 
  res.status(200).json({ status: "success", data: { result: createNewAuthor } });
};

// update Author
const updateAuthor = async (req, res, next) => {
  const { error } = validationUpdateAuthor(req.body);
  if (error) return res.status(400).json({ status: "fail", message: error.message, });
  // {1} Update author in the database
  const updateAuthor = await Author.findByIdAndUpdate(req.params.id,
    _.pick(req.body, ["name", "bio", "website"]), { new: true, runValidators: true });
  // If the author is not found
  if (!updateAuthor) return next(new AppError("Author not found", 404));
  // {2} Return a 200 response
  res.status(200).json({ status: "success", data: { result: updateAuthor } });
};

// remove Author
const deleteAuthor = async (req, res, next) => {
  // {1} Find and remove the author from the database by ID
  const removeAuthor = await Author.findByIdAndRemove(req.params.id);
  // If the author is not found
  if (!removeAuthor) return next(new AppError("Author not found", 404));
  // {2} Return a 200 response
  res.status(200).json({ status: "success", data: { result: removeAuthor } });
};

// get Author by id
const getAuthor = async (req, res, next) => {
  // {1} Find the author by ID
  const getOneAuthor = await Author.findById(req.params.id);
  // If the author is not found
  if (!getOneAuthor) return next(new AppError("Author not found", 404));
  // {2} Return a 200 response
  res.status(200).json({ status: "success", data: { result: getOneAuthor } });
};

module.exports = { getAuthors, createAuthor, updateAuthor, deleteAuthor, getAuthor, };
