const _ = require("lodash");
const AppError = require("./../middleware/classError");
const { User } = require("./../models/userModel");
const { validateUserUpdate } = require("./../middleware/errorJoi");

const mySelect = "-role -active -password -__v";
// Get all users and paginate the results
const getAllUsers = async (req, res) => {
    const page = req.query.page * 1 || 1;
    const limit = req.query.limit * 1 || 100;
    const skip = (page - 1) * limit;
    // {1} Find all users in the database
    const user = await User.find().limit(limit).skip(skip).select(mySelect);
    const count = user.length;
    // {2} Return a 200 response
    res.status(200).json({ sataus: "success", count, page, data: { user } });
}

// Update user information
const updateUser = async (req, res, next) => {
    const { error } = validateUserUpdate(req.body);
    if (error) {
        return res.status(400).json({ status: "fail", message: error.message });
    }
    filterBody = _.pick(req.body, ["username", "email"]);
    if (req.body.password || req.body.passwordConfirm) {
        return next(new AppError("this route is not for password update", 400));
    }
    // {1} Find and update the user in the database
    const updateUser = await User.findByIdAndUpdate(req.user.id, filterBody, {
        new: true, runValidators: true
    }).select(mySelect);
    // {2} Return a 200 response
    res.status(200).json({ sataus: "success", data: { updateUser } });
}

// Deactivate user account
const deleteUser = async (req, res, next) => {
    // {1} Find and update the user's active status in the database
    const deleteUser = await User.findByIdAndUpdate(req.user.id, { active: false }).select(mySelect);
    if (!deleteUser) { return next(new AppError("course not found", 404)) }
    // {2} Return a 200 response
    res.status(200).json({ status: "success", data: null });
}
module.exports = { getAllUsers, updateUser, deleteUser };