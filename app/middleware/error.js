const AppError = require("./classError");

const handleCastErrorDB = (err) => {
  const message = `Invalid ${err.path}: ${err.value}`;
  return new AppError(message, 400);
};

const handleDuplicateFieldsDB = (err) => {
  const value = err.errmsg.match(/(["'])(?:(?=(\\?))\2.)*?\1/)[0];
  const message = `Duplicate field value ${value}. Please use another value!`;
  return new AppError(message, 400);
};
const handleValidationErrorDB = (err) => {
  const errors = Object.values(err.errors).map((ele) => ele.message);
  const message = `Invalid input data ${errors.join(`  *- `)}`;
  return new AppError(message, 400);
};
const handleJWTErrorDB = () => new AppError("Invalid Token. Please log in again!", 401)
const JsonJWTExpiredError = () => new AppError("your Token has expired. Please log in again!", 401)

const sendErrorDev = (err, res) => {
  res.status(err.statusCode).json({
    status: err.status,
    error: err,
    message: err.message,
    stack: err.stack,
  });
};
const sendErrorProd = (err, res) => {
  res.status(err.statusCode).json({
    status: err.status,
    message: err.message,
  });
};

module.exports = function (err, req, res, next) {
  err.statusCode = err.statusCode || 500;
  err.status = err.status || "error";

  if (process.env.NODE_ENV === "development") {
    sendErrorDev(err, res);
  } else if (process.env.NODE_ENV === "production") {
    if (err.name === "CastError") err = handleCastErrorDB({ ...err });
    if (err.code === 11000) err = handleDuplicateFieldsDB({ ...err });
    if (err.name === "ValidationError") err = handleValidationErrorDB({ ...err });
    if (err.name === "JsonWebTokenError") err = handleJWTErrorDB();
    if (err.name === "TokenExpiredError") err = JsonJWTExpiredError();

    // Send the error response in production mode
    sendErrorProd(err, res);
  }
};
