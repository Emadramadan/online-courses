const jwt = require("jsonwebtoken");
const AppError = require("./classError");
const { User } = require("../models/userModel");

module.exports = async function (req, res, next) {
  const token = req.cookies.authorization;
  if (!token) return next(new AppError("Access denied. No token provided", 401));
  // Verification token
  const decoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
  // check if user still exists
  const currentUser = await User.findById(decoded.id);
  if (!currentUser) return next("The Token belonging to this user does no longer exist.", 401);
  // check if user changed password after the token was issued
  // if (currentUser.changedPasswordAfter(decoded.iat)) {
  //   return next("User recently changed Password! Please log in again.", 401);
  // }

  // Grant access To protected Route
  req.user = decoded;
  next();
};
