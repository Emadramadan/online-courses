const Joi = require("joi");

// Author Validation For Create and Update
function validationCreateAuthor(param) {
    const schema = Joi.object({
        name: Joi.string().min(3).max(50).required(),
        bio: Joi.string().min(3).max(255).required(),
        website: Joi.string().min(3).max(50).required(),
    });
    return schema.validate(param);
}
function validationUpdateAuthor(param) {
    const schema = Joi.object({
        name: Joi.string().min(3).max(50),
        bio: Joi.string().min(3).max(255),
        website: Joi.string().min(3).max(50),
    });
    return schema.validate(param);
}

// Course Validation For Create and Update
function validationCreateCourse(param) {
    const schema = Joi.object({
        title: Joi.string().min(3).max(75).required(),
        description: Joi.string().min(3).max(255).required(),
        price: Joi.number().max(9999999999).min(5).required(),
        author: Joi.required(),
    });
    return schema.validate(param);
}
function validationUpdateCourse(param) {
    const schema = Joi.object({
        title: Joi.string().min(3).max(75),
        description: Joi.string().min(3).max(255),
        price: Joi.number().max(9999999999).min(5),
    });
    return schema.validate(param);
}

// Auth Validation For Login and Signup and UpdatePassword
function validateUserSignup(request) {
    const schema = Joi.object({
        username: Joi.string().min(5).max(255).alphanum().required(),
        email: Joi.string().min(5).max(255).email().required(),
        password: Joi.string().min(5).max(255)
            .pattern(new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,50}$'))
            .message('The password must contain uppercase and lowercase Latin letters, digits, and special characters')
            .required(),
        passwordConfirm: Joi.string().valid(Joi.ref('password')).required(),
    });
    return schema.validate(request);
}
function validateUserLogin(request) {
    const schema = Joi.object({
        email: Joi.string().min(5).max(255).email().required(),
        password: Joi.string().min(5).max(255).required(),
    });
    return schema.validate(request);
}
function validateUserUpdatePass(request) {
    const schema = Joi.object({
        passwordCurrent: Joi.string().required(),
        password: Joi.string().min(5).max(255)
            .pattern(new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,50}$'))
            .message('The password must contain uppercase and lowercase Latin letters, digits, and special characters')
            .required(),
        passwordConfirm: Joi.string().valid(Joi.ref('password')).required(),
    });
    return schema.validate(request);
}

// Update User Information
function validateUserUpdate(request) {
    const schema = Joi.object({
        username: Joi.string().min(5).max(255).alphanum(),
        email: Joi.string().min(5).max(255).email(),
    });
    return schema.validate(request);
}

module.exports = {
    validationCreateCourse,
    validationUpdateCourse,
    validationCreateAuthor,
    validationUpdateAuthor,
    validateUserSignup,
    validateUserLogin,
    validateUserUpdatePass,
    validateUserUpdate
};  
